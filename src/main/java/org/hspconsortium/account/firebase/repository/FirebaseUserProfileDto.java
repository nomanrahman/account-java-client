package org.hspconsortium.account.firebase.repository;


public class FirebaseUserProfileDto {
    private String email;
    private String uid;
    private String displayName;

    public String getEmail() {
        return email;
    }

    public FirebaseUserProfileDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public FirebaseUserProfileDto setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public FirebaseUserProfileDto setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
}
